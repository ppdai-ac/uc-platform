package com.ppdai.login.rest;

import com.ppdai.login.request.NomalLoginRequest;
import com.ppdai.login.response.NomalLoginResponse;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiParam;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;


/**
 * Created by yanglei on 2016/9/28.
 */
public interface NomalLogin {
    @ApiOperation(value = "NomalLogin", notes = "", response = NomalLoginResponse.class, tags = {})
    @ApiResponses(value = {
    @ApiResponse(code = 200, message = "", response = NomalLoginResponse.class)})
    @RequestMapping(value = "/NomalLogin",
            produces = {"application/json"},
            consumes = {"application/json"},
            method = RequestMethod.POST)
    ResponseEntity<NomalLoginResponse> nomalLoginPost(
            @ApiParam(value = "‘’", required = true) @RequestBody NomalLoginRequest nomalLoginRequest

    );
}
