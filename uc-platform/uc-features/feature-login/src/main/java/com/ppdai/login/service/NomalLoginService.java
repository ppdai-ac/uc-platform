package com.ppdai.login.service;

import io.swagger.annotations.ApiParam;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestBody;
import com.ppdai.login.request.NomalLoginRequest;
import com.ppdai.login.response.NomalLoginResponse;
import com.ppdai.login.rest.NomalLogin;

/**
 * Created by yanglei on 2016/9/28.
 */
@Controller
public class NomalLoginService implements NomalLogin {

    @Override
    public ResponseEntity<NomalLoginResponse> nomalLoginPost(@ApiParam(value = "‘’", required = true) @RequestBody NomalLoginRequest nomalLoginRequest) {
        return null;
    }
}
