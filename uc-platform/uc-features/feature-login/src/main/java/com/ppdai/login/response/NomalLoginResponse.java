package com.ppdai.login.response;

import io.swagger.annotations.ApiModelProperty;


/**
 * Created by yanglei on 2016/9/28.
 */

public class NomalLoginResponse   {
    private Integer returnCode = null;

    private String returnMessage = null;

    public NomalLoginResponse returnCode(Integer returnCode) {
        this.returnCode = returnCode;
        return this;
    }

    /**
     * Get returnCode
     * @return returnCode
     **/
    @ApiModelProperty(value = "")
    public Integer getReturnCode() {
        return returnCode;
    }

    public void setReturnCode(Integer returnCode) {
        this.returnCode = returnCode;
    }

    public NomalLoginResponse returnMessage(String returnMessage) {
        this.returnMessage = returnMessage;
        return this;
    }

    /**
     * Get returnMessage
     * @return returnMessage
     **/
    @ApiModelProperty(value = "")
    public String getReturnMessage() {
        return returnMessage;
    }

    public void setReturnMessage(String returnMessage) {
        this.returnMessage = returnMessage;
    }

}