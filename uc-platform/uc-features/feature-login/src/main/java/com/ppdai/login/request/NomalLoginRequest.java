package com.ppdai.login.request;

import io.swagger.annotations.ApiModelProperty;

import java.util.HashMap;
import java.util.Map;

/**
 * Created by yanglei on 2016/9/28.
 */

public class NomalLoginRequest   {
    private String accountName = null;

    private String accountPassword = null;

    private String imgValidCode = null;

    private String isValidateImgCode = null;

    private String loginSource = null;

    private String uniqueId = null;

    private Map<String, String> extraInfo = new HashMap<String, String>();

    public NomalLoginRequest accountName(String accountName) {
        this.accountName = accountName;
        return this;
    }

    /**
     * Get accountName
     * @return accountName
     **/
    @ApiModelProperty(value = "")
    public String getAccountName() {
        return accountName;
    }

    public void setAccountName(String accountName) {
        this.accountName = accountName;
    }

    public NomalLoginRequest accountPassword(String accountPassword) {
        this.accountPassword = accountPassword;
        return this;
    }

    /**
     * Get accountPassword
     * @return accountPassword
     **/
    @ApiModelProperty(value = "")
    public String getAccountPassword() {
        return accountPassword;
    }

    public void setAccountPassword(String accountPassword) {
        this.accountPassword = accountPassword;
    }

    public NomalLoginRequest imgValidCode(String imgValidCode) {
        this.imgValidCode = imgValidCode;
        return this;
    }

    /**
     * Get imgValidCode
     * @return imgValidCode
     **/
    @ApiModelProperty(value = "")
    public String getImgValidCode() {
        return imgValidCode;
    }

    public void setImgValidCode(String imgValidCode) {
        this.imgValidCode = imgValidCode;
    }

    public NomalLoginRequest isValidateImgCode(String isValidateImgCode) {
        this.isValidateImgCode = isValidateImgCode;
        return this;
    }

    /**
     * Get isValidateImgCode
     * @return isValidateImgCode
     **/
    @ApiModelProperty(value = "")
    public String getIsValidateImgCode() {
        return isValidateImgCode;
    }

    public void setIsValidateImgCode(String isValidateImgCode) {
        this.isValidateImgCode = isValidateImgCode;
    }

    public NomalLoginRequest loginSource(String loginSource) {
        this.loginSource = loginSource;
        return this;
    }

    /**
     * Get loginSource
     * @return loginSource
     **/
    @ApiModelProperty(value = "")
    public String getLoginSource() {
        return loginSource;
    }

    public void setLoginSource(String loginSource) {
        this.loginSource = loginSource;
    }

    public NomalLoginRequest uniqueId(String uniqueId) {
        this.uniqueId = uniqueId;
        return this;
    }

    /**
     * Get uniqueId
     * @return uniqueId
     **/
    @ApiModelProperty(value = "")
    public String getUniqueId() {
        return uniqueId;
    }

    public void setUniqueId(String uniqueId) {
        this.uniqueId = uniqueId;
    }

    public NomalLoginRequest extraInfo(Map<String, String> extraInfo) {
        this.extraInfo = extraInfo;
        return this;
    }

    public NomalLoginRequest putExtraInfoItem(String key, String extraInfoItem) {
        this.extraInfo.put(key, extraInfoItem);
        return this;
    }

    /**
     * Get extraInfo
     * @return extraInfo
     **/
    @ApiModelProperty(value = "")
    public Map<String, String> getExtraInfo() {
        return extraInfo;
    }

    public void setExtraInfo(Map<String, String> extraInfo) {
        this.extraInfo = extraInfo;
    }


}
